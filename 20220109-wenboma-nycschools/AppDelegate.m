//
//  AppDelegate.m
//  20220109-wenboma-nycschools
//
//  Created by Wenbo Ma on 1/9/22.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    ViewController *home = [[ViewController alloc] init];
    _navController = [[UINavigationController alloc] initWithRootViewController:home];
    
    [_navController.navigationBar setBackgroundColor:[UIColor blueColor]];
    [self.window setRootViewController:_navController];
    
    [self.window makeKeyAndVisible];
    return YES;
}

@end
