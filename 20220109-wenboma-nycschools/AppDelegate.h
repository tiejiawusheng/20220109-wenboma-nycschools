//
//  AppDelegate.h
//  20220109-wenboma-nycschools
//
//  Created by Wenbo Ma on 1/9/22.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@end

