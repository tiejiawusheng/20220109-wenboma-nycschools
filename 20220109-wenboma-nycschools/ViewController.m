//
//  ViewController.m
//  20220109-wenboma-nycschools
//
//  Created by Wenbo Ma on 1/9/22.
//

#import "ViewController.h"
#import "DetailViewController.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = UIColor.grayColor;
    
    [self readCSVData];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

// Every Section Cell number
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.allLinedStrings count];
}
// Set devery Cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Create a cellID，use cell reuse
    NSString *cellID = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellID];
    }
    
    NSString *first = [[[self.allLinedStrings objectAtIndex:(int)indexPath.row] componentsSeparatedByString:@","] objectAtIndex:0];
    
    NSString *second = [[[self.allLinedStrings objectAtIndex:(int)indexPath.row] componentsSeparatedByString:@","] objectAtIndex:1];
    
    NSString *third = [[[self.allLinedStrings objectAtIndex:(int)indexPath.row] componentsSeparatedByString:@","] objectAtIndex:2];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", first];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@   %@", second,third];
    cell.detailTextLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * schoolDBN = [[[self.allLinedStrings objectAtIndex:(int)indexPath.row] componentsSeparatedByString:@","] objectAtIndex:0];
    DetailViewController * detailView = [[DetailViewController alloc] initWithUserID:schoolDBN];
    [self.navigationController pushViewController:detailView animated:YES];
}

-(void)readCSVData{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"2017_DOE_High_School_Directory" ofType:@"csv"];
    NSError *error = nil;
    NSString *fileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    
    NSArray * temp = [fileContents componentsSeparatedByString:@"\r\n"];
    self.allLinedStrings = [[temp objectAtIndex:0] componentsSeparatedByString:@"\n"];
}

@end
