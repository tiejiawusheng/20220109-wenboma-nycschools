//
//  DetailViewController.h
//  20220109-wenboma-nycschools
//
//  Created by Wenbo Ma on 1/9/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController

-(id) initWithUserID:(NSString *)userID;

@property (nonatomic,retain) UILabel *DBNData;
@property (nonatomic,retain) UILabel *schoolNameData;
@property (nonatomic,retain) UILabel *numData;
@property (nonatomic,retain) UILabel *readingData;
@property (nonatomic,retain) UILabel *mathData;
@property (nonatomic,retain) UILabel *writingData;


@property (nonatomic,retain) NSString * schoolDBN;
@property (nonatomic,retain) NSString * DBNString;
@property (nonatomic,retain) NSString * schoolNameString;
@property (nonatomic,retain) NSString * numString;
@property (nonatomic,retain) NSString * readingString;
@property (nonatomic,retain) NSString * mathString;
@property (nonatomic,retain) NSString * writingString;
@property (nonatomic,retain) NSArray  * satScore;

@end

NS_ASSUME_NONNULL_END
