//
//  DetailViewController.m
//  20220109-wenboma-nycschools
//
//  Created by Wenbo Ma on 1/9/22.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

-(id) initWithUserID:(NSString *)schoolDBN;
{
    self = [super init];
    if (self) {
        
        self.schoolDBN = schoolDBN;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"SAT SCORE";
    
    // Do any additional setup after loading the view.
    [self readCSVData];
    [self addTitleDetail];
    [self getDetailSATScore];
}

-(void) addTitleDetail {
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    UILabel *DBN = [[UILabel alloc] initWithFrame:CGRectMake(0, 300, 50, 100)];
    DBN.backgroundColor = UIColor.grayColor;
    DBN.textColor = UIColor.blackColor;
    DBN.font=[UIFont systemFontOfSize:10];
    DBN.textAlignment = NSTextAlignmentCenter;
    DBN.text = @"DBN";
    [self.view addSubview:DBN];
    
    _DBNData = [[UILabel alloc] initWithFrame:CGRectMake(0, 400, 50, 100)];
    _DBNData.backgroundColor = UIColor.grayColor;
    _DBNData.textColor = UIColor.blackColor;
    _DBNData.font=[UIFont systemFontOfSize:10];
    _DBNData.textAlignment = NSTextAlignmentCenter;
    _DBNData.text = @"";
    [self.view addSubview:_DBNData];
    
    UILabel *schoolName = [[UILabel alloc] initWithFrame:CGRectMake(50, 300, 160, 100)];
    schoolName.backgroundColor = UIColor.grayColor;
    schoolName.textColor = UIColor.blackColor;
    schoolName.font=[UIFont systemFontOfSize:10];
    schoolName.textAlignment = NSTextAlignmentCenter;
    schoolName.text = @"SHOOL_NAME";
    [self.view addSubview:schoolName];
    
    _schoolNameData = [[UILabel alloc] initWithFrame:CGRectMake(50, 400, 160, 100)];
    _schoolNameData.backgroundColor = UIColor.grayColor;
    _schoolNameData.textColor = UIColor.blackColor;
    _schoolNameData.font=[UIFont systemFontOfSize:10];
    _schoolNameData.textAlignment = NSTextAlignmentCenter;
    _schoolNameData.text = @"";
    [self.view addSubview:_schoolNameData];
    
    UILabel *num = [[UILabel alloc] initWithFrame:CGRectMake(210, 300, 50, 100)];
    num.backgroundColor = UIColor.grayColor;
    num.textColor = UIColor.blackColor;
    num.font=[UIFont systemFontOfSize:10];
    num.textAlignment = NSTextAlignmentCenter;
    num.text = @"Num";
    [self.view addSubview:num];
    
    _numData = [[UILabel alloc] initWithFrame:CGRectMake(210, 400, 50, 100)];
    _numData.backgroundColor = UIColor.grayColor;
    _numData.textColor = UIColor.blackColor;
    _numData.font=[UIFont systemFontOfSize:10];
    _numData.textAlignment = NSTextAlignmentCenter;
    _numData.text = @"";
    [self.view addSubview:_numData];
    
    UILabel *reading = [[UILabel alloc] initWithFrame:CGRectMake(260, 300, 50, 100)];
    reading.backgroundColor = UIColor.grayColor;
    reading.textColor = UIColor.blackColor;
    reading.font=[UIFont systemFontOfSize:10];
    reading.textAlignment = NSTextAlignmentCenter;
    reading.text = @"Read";
    [self.view addSubview:reading];
    
    _readingData = [[UILabel alloc] initWithFrame:CGRectMake(260, 400, 50, 100)];
    _readingData.backgroundColor = UIColor.grayColor;
    _readingData.textColor = UIColor.blackColor;
    _readingData.font=[UIFont systemFontOfSize:10];
    _readingData.textAlignment = NSTextAlignmentCenter;
    _readingData.text = @"";
    [self.view addSubview:_readingData];
    
    UILabel *math = [[UILabel alloc] initWithFrame:CGRectMake(310, 300, 50, 100)];
    math.backgroundColor = UIColor.grayColor;
    math.textColor = UIColor.blackColor;
    math.font=[UIFont systemFontOfSize:10];
    math.textAlignment = NSTextAlignmentCenter;
    math.text = @"Math";
    [self.view addSubview:math];
    
    _mathData = [[UILabel alloc] initWithFrame:CGRectMake(310, 400, 50, 100)];
    _mathData.backgroundColor = UIColor.grayColor;
    _mathData.textColor = UIColor.blackColor;
    _mathData.font=[UIFont systemFontOfSize:10];
    _mathData.textAlignment = NSTextAlignmentCenter;
    _mathData.text = @"";
    [self.view addSubview:_mathData];
    
    UILabel *writing = [[UILabel alloc] initWithFrame:CGRectMake(360, 300, 55, 100)];
    writing.backgroundColor = UIColor.grayColor;
    writing.textColor = UIColor.blackColor;
    writing.font=[UIFont systemFontOfSize:10];
    writing.textAlignment = NSTextAlignmentCenter;
    writing.text = @"WRITING";
    [self.view addSubview:writing];
    
    _writingData = [[UILabel alloc] initWithFrame:CGRectMake(360, 400, 55, 100)];
    _writingData.backgroundColor = UIColor.grayColor;
    _writingData.textColor = UIColor.blackColor;
    _writingData.font=[UIFont systemFontOfSize:10];
    _writingData.textAlignment = NSTextAlignmentCenter;
    _writingData.text = @"";
    [self.view addSubview:_writingData];
}

-(void)readCSVData{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"2012_SAT_Results" ofType:@"csv"];
    NSError *error = nil;
    NSString *fileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    
    //get every line data
    NSArray * temp = [fileContents componentsSeparatedByString:@"\r\n"];
    _satScore = [[temp objectAtIndex:0] componentsSeparatedByString:@"\n"];
}

-(void)getDetailSATScore{
    
    NSUInteger count =  [_satScore count];
    for(int i = 1; i < count; i++){
        if([[[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString: _schoolDBN]){
            
            _DBNString = [[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:0];
            _DBNData.text = _DBNString;
            
            _schoolNameString = [[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:1];
            _schoolNameData.text = _schoolNameString;
            
            _numString = [[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:2];
            _numData.text = _numString;
            
            _readingString = [[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:3];
            _readingData.text = _readingString;
            
            
            _mathString = [[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:4];
            _mathData.text = _mathString;
            
            _writingString = [[[_satScore objectAtIndex:i] componentsSeparatedByString:@","] objectAtIndex:5];
            _writingData.text = _writingString;
        }
    }
}

@end
